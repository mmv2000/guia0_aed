#include <iostream>
#include "Coordenada.h"
using namespace std;

#ifndef ATOMO_H
#define ATOMO_H

class Atomo {
	private:
		string nombre;
		int numero;
		Coordenada coordenada;
	
	public:
		// contructores de la clase
		Atomo(string nombre, int numero);
		
		// metodos get y set de la clase
		string get_nombre();
		int get_numero();
		Coordenada get_coordenada();
		void set_nombre(string nombre);
		void set_numero(int numero);
		void set_coord(Coordenada coordenada);
};
#endif

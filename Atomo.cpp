/*
  * g++ Atomo.cpp -o Atomo
 */

#include <iostream>
#include "Atomo.h"
#include "Coordenada.h"
using namespace std;

// contructores de la clase	
Atomo::Atomo (string nombre, int numero) {
	this-> nombre = nombre;
	this-> numero = numero;
}

// metodos get y set de la clase
string Atomo::get_nombre() {
	return this-> nombre;
}

int Atomo::get_numero() {
	return this-> numero;
}

Coordenada Atomo::get_coordenada() {
	return this-> coordenada;
}

void Atomo::set_nombre(string nombre) {
	this-> nombre = nombre;
}

void Atomo::set_numero(int numero) {
	this-> numero = numero;
}

void Atomo::set_coord(Coordenada coordenada) {
	this-> coordenada = coordenada;
}

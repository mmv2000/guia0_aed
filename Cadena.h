#include <list>
#include <iostream>
#include "Aminoacido.h"
using namespace std;

#ifndef CADENA_H
#define CADENA_H

class Cadena {
	private:
		string letra;
		list<Aminoacido> aminoacido;
		
	public:
		// contructores de la clase
		Cadena();
		Cadena(string letra);
		
		// metodos get y set de la clase
		string get_letra();
		list<Aminoacido> get_aminoacido();
		void set_letra(string letra);
		void add_aminoacidos(Aminoacido aminoacidos);
};
#endif

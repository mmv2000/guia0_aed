/*
  * g++ Cadena.cpp -o Cadena
 */

#include <list>
#include <iostream>
#include "Cadena.h"
#include "Aminoacido.h"
using namespace std;

// contructores de la clase
Cadena::Cadena (string letra) {
	this-> letra = letra;
	list<Aminoacido> aminoacido;
}

// metodos get y set de la clase
string Cadena::get_letra() {
	return this-> letra;
}

list <Aminoacido> Cadena::get_aminoacido() {
	return this-> aminoacido;
}

void Cadena::set_letra(string letra) {
	this->letra = letra;
}

void Cadena::add_aminoacidos(Aminoacido aminoacido) {
	this-> aminoacido.push_back (aminoacido);
}

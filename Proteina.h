#include <list>
#include <iostream>
#include "Cadena.h"
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H	

class Proteina {
	private:
		string nombre;
		string id;
		list<Cadena> cadena;
		
	public:
		// contructores de la clase
		//Proteina();
		Proteina(string nombre, string id);
		
		// métodos get y set de la clase
		string get_nombre();
		string get_id();
		list<Cadena> get_cadena();
		void set_nombre(string nombre);
		void set_id(string id);
		void add_cadena(Cadena cadena);
};
#endif

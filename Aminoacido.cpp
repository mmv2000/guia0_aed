/*
  * g++ Aminoacido.cpp -o Aminoacido
 */

#include <list>
#include <iostream>
#include "Aminoacido.h"
#include "Atomo.h"
using namespace std;

// contructores de la clase
Aminoacido::Aminoacido (string nombre, int numero) {
	this-> nombre = nombre;
	this-> numero = numero;
	list<Atomo> atomos;
}

// metodos get y set de la clase
string Aminoacido::get_nombre() {
	return this-> nombre;
}

int Aminoacido::get_numero() {
	return this-> numero;
}

list <Atomo> Aminoacido::get_atomos() {
	return this-> atomos;
}

void Aminoacido::set_nombre(string nombre) {
	this->nombre = nombre;
}

void Aminoacido::set_numero(int numero) {
	this->numero = numero;
}

void Aminoacido::add_atomo(Atomo atomos) {
	this-> atomos.push_back (atomos);
}

#include <list>
#include <iostream>
#include "Atomo.h"
using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido {
	private:
		string nombre;
		int numero;
		list<Atomo> atomos;
	
	public:
		// contructores de la clase
		Aminoacido();
		Aminoacido(string nombre, int numero);
		
		// metodos get y set de la clase
		string get_nombre();
		int get_numero();
		list<Atomo> get_atomos();
		void set_nombre(string nombre);
		void set_numero(int numero);
		void add_atomo(Atomo atomos);
};
#endif

/*
  * g++ Proteina.cpp -o Proteina
 */

#include <list>
#include <iostream>
#include "Proteina.h"

using namespace std;

// contructores de la clase
Proteina::Proteina (string nombre, string id) {
	this-> nombre = nombre;
	this-> id = id;
	list<Cadena> cadena;
}

// métodos get y set de la clase
string Proteina::get_nombre() {
	return this-> nombre;
}

string Proteina::get_id() {
	return this-> id;
}

list <Cadena> Proteina::get_cadena() {
	return this-> cadena;
}

void Proteina::set_nombre(string nombre) {
	this-> nombre = nombre;
}

void Proteina::set_id(string id) {
	this->id = id;
}

void Proteina::add_cadena(Cadena cadena) {
	this-> cadena.push_back (cadena);
}

/*
  * g++ programa.cpp -o programa
 */

#include <list>
#include <iostream>
//#include "Proteina.cpp"
#include "Proteina.h"
using namespace std;

// objetos del objeto proteina
void leer_datos_proteinas(list<Proteina> proteinas) {
	for (Proteina i: proteinas){
		cout << "NOMBRE DE LA PROTEINA:" << i.get_nombre() << endl;
		cout << "ID:" << i.get_id() << endl;
		list<Cadena> cadena = i.get_cadena();
		for (Cadena j:cadena){
			cout << "CADENA:" << j.get_letra() << endl;
			list<Aminoacido> aminoacidos = j.get_aminoacido();
			for (Aminoacido k: aminoacidos){
				cout << "NOMBRE AMINOACIDO:" << k.get_nombre() << endl;
				cout << "AMINOACIDO NUMERO:" << k.get_numero() << endl;
				list<Atomo> atoms = k.get_atomos();
				for (Atomo l: atoms){
					cout << "ATOMO: " << l.get_nombre() << endl;
					cout << "ATOMO NUMERO:: " << l.get_numero() << endl;
					Coordenada posicion = l.get_coordenada();
						cout << "POSICION X:" << posicion.get_x() << endl;
						cout << "POSICION Y:" << posicion.get_y() << endl;
						cout << "POSICION Z:" << posicion.get_z() << endl;
						
					}
				}
			}
		}
}

int main(int argc, char **argv)
{	
	string nombre;
	string id;
	string cadena;
	string aminoacido;
	int pos_aa;
	string atomo;
	int pos_atom;
	int coord_x;
	int coord_y;
	int coord_z;
	list<Proteina> proteinas;
	int num;
	
	// Menu proteinas
	
	cout <<"BIENVENIDO AL PROGRAMA PROTEINAS:" << endl;
	cout <<"Para acceder al programa pulse 1, de lo contrario pulse 0:"<< endl;
	cin >> num;
	
	if (num == 1){
		cout <<"Ingrese el nombre de la proteina:"<< endl;
		cin >> nombre;
		cout <<"Ingrese el id de la proteina:"<< endl;
		cin >> id;
		cout <<"Ingrese cadena de la proteina:"<< endl;
		cin >> cadena;
		cout <<"Ingrese aminoacido de la cadena:"<< endl;
		cin >> aminoacido;
		cout <<"Ingrese posicion del aminoacido:"<< endl;
		cin >> pos_aa;
		cout <<"Ingrese atomo del aminoacido:"<< endl;
		cin >> atomo;
		cout <<"Ingrese lugar del atomo en el aminoacido:"<< endl;
		cin >> pos_atom;
		cout <<"Ingrese coordenada x del atomo:"<< endl;
		cin >> coord_x;
		cout <<"Ingrese coordenada y del atomo:"<< endl;
		cin >> coord_y;
		cout <<"Ingrese coordenada z del atomo:"<< endl;
		cin >> coord_z;
		Proteina p = Proteina(nombre, id);
		Cadena c = Cadena(cadena);
		Aminoacido aa = Aminoacido(aminoacido, pos_aa);
		Atomo a = Atomo(atomo, pos_atom);
		Coordenada co = Coordenada(coord_x, coord_y, coord_z);
		a.set_coord(co);
		aa.add_atomo(a);
		c.add_aminoacidos(aa);
		p.add_cadena(c);
		proteinas.push_back (p);
		leer_datos_proteinas(proteinas);
	}
	else {
		cout << "HASTA PRONTO" << endl;
}
	return 0;
}

